require 'json'
require 'rubygems'
require 'twitter'
require 'twemoji'
require './authentication.rb'
require './tweets.rb'

auth = Authentication.new
rclient = auth.rclient
# sclient = auth.sclient
read_tweets = Tweets.new

def mention(tweet)
  mention = "@#{tweet.user.screen_name}"

  tweet.user_mentions.each do |user|
    mention += " @#{user.screen_name}"
  end
  mention.slice!('@senorobot')
  mention
end

def keyword(corpus, tweet_text)
  corpus.each do |key, value|
    value.each do |keyword|
      return key if tweet_text.downcase.include?(keyword)
    end
  end
end

def filter(keyword, rclient, replies, tweet)
  if keyword == 'stopmencion' || tweet.retweeted_status?
    puts 'No contestar.'
  elsif replies[keyword]
    mention = mention(tweet)
    reply = "#{mention} #{replies[keyword].sample}"
    puts "#{keyword}: #{reply}"
    sleep(rand(8..15))
    rclient.update(reply.to_s, 'in_reply_to_status_id' => tweet.id)
  else
    mention = mention(tweet)
    reply = "#{mention} #{replies['general'].sample}"
    puts "general: #{reply}"
    sleep(rand(8..15))
    rclient.update(reply.to_s, 'in_reply_to_status_id' => tweet.id)
  end
end

# Search API
rclient.search('-from:senorobot @senorobot -rt', since_id: read_tweets.last_tweet).each do |tweet|
  tweet_text = tweet.text
  corpus_file = File.read('corpus.json')
  replies_file = File.read('replies.json')
  corpus = JSON.parse(corpus_file)
  replies = JSON.parse(replies_file)

  keyword = keyword(corpus, tweet_text)
  filter(keyword, rclient, replies, tweet)

  read_tweets.insert_tweet(tweet.id)
end

rclient.search('-from:senorobot tu bot @buenapava -rt', since_id: read_tweets.last_tweet).each do |tweet|
  # tweet_text = tweet.text
  # corpus_file = File.read('corpus.json')
  replies_file = File.read('replies.json')
  # corpus = JSON.parse(corpus_file)

  replies = JSON.parse(replies_file)
  filter('turing', rclient, replies, tweet)

  read_tweets.insert_tweet(tweet.id)
end
